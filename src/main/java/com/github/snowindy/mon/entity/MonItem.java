package com.github.snowindy.mon.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class MonItem {

    private String url;

    private String title;

    private String textBody;

    private Map<String, String> metadata = new HashMap<String, String>();

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTextBody() {
        return textBody;
    }

    public void setTextBody(String textBody) {
        this.textBody = textBody;
    }

    public String getFullText() {
        return title + " " + textBody;
    }

    public String getClientInfo() {
        return getByPrefix("CLIENT");
    }

    public String getJobInfo() {
        return getByPrefix("JOB");
    }

    private String getByPrefix(String prefix) {
        List<String> items = new ArrayList<String>();
        for (Map.Entry<String, String> entry : metadata.entrySet()) {
            MetadataKeys k = MetadataKeys.valueOf(entry.getKey());
            if (k.name().startsWith(prefix)) {
                if (StringUtils.isNoneBlank(entry.getValue())) {
                    items.add(String.format("%s: %s", MetadataKeys.valueOf(entry.getKey()).getText(), entry.getValue()));
                }
            }
        }
        Collections.sort(items);
        return StringUtils.join(items, "\n");
    }

    public String getPlainTextForm() {

        return "Title: " + title + "\n\n-------------------------------------\n\n" + textBody
                + "\n\n-------------------------------------\n\n" + getJobInfo()
                + "\n\n-------------------------------------\n\n" + getClientInfo();
    }

}
