package com.github.snowindy.mon.entity;

public enum MetadataKeys {
    CLIENT_COUNTRY("Country"), CLIENT_AVATAR_URL("Avatar url"), CLIENT_COUNTRY_CODE("Country code"), CLIENT_GEO_STATE(
            "State"), CLIENT_CITY("City"), CLIENT_FEEDBACK_RATING("Rating"), CLIENT_REVIEWS_COUNT("Reviews count"), CLIENT_JOBS_POSTED(
            "Jobs posted"), CLIENT_TOTAL_SPENT("Total spent"), CLIENT_PAST_HIRES("Past hires"), CLIENT_PAYMENT_VERIFICATION_STATUS(
            "Payment status"), CLIENT_NAME("Client name"), CLIENT_REGISTER_DATE("Registration date"),

    JOB_TYPE("Job type"), JOB_BUDGET("Budget"), JOB_DURATION("Duration"), JOB_WORKLOAD("Workload"), JOB_CATEGORY(
            "Category"), JOB_SKILLS("Skills"), JOB_TAGS("Tags"), JOB_FORMATTED_MAX_BUDGET("Max budget"), JOB_FORMATTED_MAX_RATE(
            "Max rate"), JOB_FORMATTED_MIN_BUDGET("Min budget"), JOB_FORMATTED_MIN_RATE("Min rate");

    private String text;

    private MetadataKeys(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
