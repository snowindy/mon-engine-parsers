package com.github.snowindy.mon.i.parse;

import java.util.List;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;

public interface ITopicPageParser extends IParser {

    List<MonItem> getMonItems() throws NotAvailableException;
    
    boolean isTopicMonItemsMustHaveBody();

}
