package com.github.snowindy.mon.i.parse;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;

public interface IMonItemPageParser extends IParser {
    MonItem getJob() throws NotAvailableException;
}
