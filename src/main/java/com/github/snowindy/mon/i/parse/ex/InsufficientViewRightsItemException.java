package com.github.snowindy.mon.i.parse.ex;

/**
 * Some marketplaces don't allow to view item page for anonimous/non-priveleged
 * users.
 */
public class InsufficientViewRightsItemException extends NotAvailableException {

    private static final long serialVersionUID = 1L;

    public InsufficientViewRightsItemException(String sourceDescriptor) {
        super("Insufficient view rights for this item", sourceDescriptor);
    }

}
