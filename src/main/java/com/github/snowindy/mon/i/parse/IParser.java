package com.github.snowindy.mon.i.parse;

public interface IParser {
    void setRelativeUrlPrefix(String prefix);

    void parse(String pageText, String sourceDescriptor);
}
