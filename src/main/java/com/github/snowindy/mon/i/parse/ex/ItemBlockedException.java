package com.github.snowindy.mon.i.parse.ex;

/**
 * Some marketplaces show item in the list, but when you move to item page, it
 * appears to be blocked by site admin.
 * 
 */
public class ItemBlockedException extends NotAvailableException {

    private static final long serialVersionUID = 1L;

    public ItemBlockedException(String sourceDescriptor) {
        super("Item is blocked", sourceDescriptor);
    }

}
