package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class FreelancehuntComSiteItemParser extends AbstractXmlParser implements IMonItemPageParser {

    private final static Logger logger = LoggerFactory.getLogger(FreelancehuntComSiteItemParser.class);

    @Override
    public MonItem getJob() throws NotAvailableException {
        try {
            MonItem job = new MonItem();

            Element descrEl = null;
            String catText = null;

            Element title = doc.select("h1 .visible-xs").first();
            job.setTitle(replaceHtmlSpecials(title.text()));

            descrEl = doc.select(".container .row .linkify-marker").first();

            catText = getCatText();


            String descrText = StringUtil.escapeHtmlWithNewLineSupport(descrEl.html());
            descrText = replaceHtmlSpecials(descrText);

            if (StringUtils.isNotBlank(catText)) {
                descrText += DESCR_SECTION_SEPARATOR + catText;
            }

            job.setTextBody(descrText);

            return job;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    String getCatText() {
        List<String> names = new ArrayList<>();
        for (Element el : doc.select(".col-md-12 .smaller a")) {
            names.add(el.text());
        }

        String res = StringUtils.join(names, ", ");
        if (StringUtils.isNotBlank(res)) {
            return "Разделы: " + res;
        }
        return null;
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
