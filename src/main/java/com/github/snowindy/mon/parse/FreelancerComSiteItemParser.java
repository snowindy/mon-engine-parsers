package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.github.snowindy.mon.entity.MetadataKeys;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class FreelancerComSiteItemParser extends AbstractXmlParser implements IMonItemPageParser {

    private final static Logger logger = LoggerFactory.getLogger(FreelancerComSiteItemParser.class);

    @Override
    public MonItem getJob() throws NotAvailableException {
        try {
            MonItem job = new MonItem();

            Element title = doc.select(".project-view-project-title").first();
            job.setTitle(replaceHtmlSpecials(title.text()));

            String descrText = "";
            Iterator<Element> it = doc.select(".project-view-inner p").iterator();
            while (it.hasNext()){
                descrText += "\n"+ replaceHtmlSpecials(StringUtil.escapeHtmlWithNewLineSupport(it.next().html()));
            }
            descrText = StringUtils.trim(descrText);
            job.setTextBody(descrText);

            Element bel = doc.select(".project-statistic-value").first();
            if (bel != null){
                job.getMetadata().put(MetadataKeys.JOB_BUDGET.name(), replaceHtmlSpecials(StringUtil.escapeHtmlWithNewLineSupport(bel.html())));
            }

            it = doc.select(".project-view-landing-required-skill .simple-tag").iterator();
            List<String> skills= new ArrayList<>();
            while (it.hasNext()){
                skills.add(replaceHtmlSpecials(StringUtil.escapeHtmlWithNewLineSupport(it.next().html())));
            }
            job.getMetadata().put(MetadataKeys.JOB_SKILLS.name(), StringUtils.join(skills, ", "));

            return job;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
