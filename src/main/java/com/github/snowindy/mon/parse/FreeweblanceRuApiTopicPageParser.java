package com.github.snowindy.mon.parse;

import com.github.snowindy.mon.entity.MetadataKeys;
import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;
import com.github.snowindy.util.text.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FreeweblanceRuApiTopicPageParser extends AbstractXmlParser implements ITopicPageParser {
    @Override
    public List<MonItem> getMonItems() {
        try {
            Elements jobItems = doc.select("channel item");

            List<MonItem> res = new ArrayList<MonItem>();

            int idx = 0;
            for (Element jobItem : jobItems) {

                MonItem item = new MonItem();
                item.setTitle(jobItem.select("title").first().text());
                item.setUrl(jobItem.select("lmlink").first().text());

                String descrHtml = jobItem.select("description").first().text();
                descrHtml = StringUtil.escapeHtmlWithNewLineSupport(descrHtml);
                descrHtml = replaceHtmlSpecials(descrHtml);

                item.setTextBody(descrHtml);

                res.add(item);
            }

            return res;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String parseInner(String pageContent) {
        // Jsoup does not understand 'link' tag, WTF
        return pageContent.replace("<link><![CDATA[","<lmlink><![CDATA[").replace("]]></link>", "]]></lmlink>");
    }

    private final static Logger logger = LoggerFactory.getLogger(FreeweblanceRuApiTopicPageParser.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean isTopicMonItemsMustHaveBody() {
        return true;
    }

}
