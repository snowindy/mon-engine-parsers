package com.github.snowindy.mon.parse;

import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class PeopleperhourComSiteItemParser extends AbstractXmlParser implements IMonItemPageParser {

    private final static Logger logger = LoggerFactory.getLogger(PeopleperhourComSiteItemParser.class);

    @Override
    public MonItem getJob() throws NotAvailableException {
        try {
            MonItem job = new MonItem();

            Element descrEl = null;

            Element title = doc.select(".main-content h1").first();
            job.setTitle(replaceHtmlSpecials(title.text()));

            descrEl = doc.select(".job-details div").first();

            String descrText = StringUtil.escapeHtmlWithNewLineSupport(descrEl.html());
            descrText = replaceHtmlSpecials(descrText);

            job.setTextBody(descrText);

            return job;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
