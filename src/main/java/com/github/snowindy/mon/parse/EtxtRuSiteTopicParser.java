package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class EtxtRuSiteTopicParser extends AbstractXmlParser implements
		ITopicPageParser {
	
	private final static Logger logger = LoggerFactory.getLogger(EtxtRuSiteTopicParser.class);
	
	@Override
	public List<MonItem> getMonItems() throws NotAvailableException {
		try {
			
			Elements jobPrevItems = doc.select("div.mod-item");
            
            List<MonItem> res = new ArrayList<MonItem>();
            
            for (Element jobPrevItem : jobPrevItems) {
                Element headerElLink = jobPrevItem.select(".mod-item-main h4").first();

                MonItem prev = new MonItem();
                String title = headerElLink.text();
				prev.setTitle(StringUtil.whitespaceClenup(StringUtils.trim(title)));
                prev.setUrl(headerElLink.select("a[href]").attr("href"));
                
                String body = extractTextBody(jobPrevItem);
                
                prev.setTextBody(body);
                
                res.add(prev);
            }

            return res;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
	}

	private String extractTextBody(Element jobPrevItem) {
		String body = jobPrevItem.select("div.mod-item-text span").first().html();
		body = StringUtil.escapeHtmlWithNewLineSupport(body);
		body = replaceHtmlSpecials(body);
		
		String category = extractCategoryName(jobPrevItem);
		
		if (StringUtils.isNotBlank(category)) {
		    body += DESCR_SECTION_SEPARATOR + category;
		}
		return body;
	}

	private String extractCategoryName(Element jobPrevItem) {
		Element categoryElement = jobPrevItem.select(".mod-item-category").first();
		String category = categoryElement.text();
		if (category.startsWith("--")) {
			category = "";
		} else {
			category = "Раздел: " + category;
		}
		return category;
	}
	
	@Override
	public boolean isTopicMonItemsMustHaveBody() {
		return true;
	}

	@Override
	protected Logger getLogger() {
		return logger;
	}
	
}
