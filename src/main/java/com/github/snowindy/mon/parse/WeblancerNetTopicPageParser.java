package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;

public class WeblancerNetTopicPageParser extends AbstractXmlParser implements ITopicPageParser {

    private final static Logger logger = LoggerFactory.getLogger(WeblancerNetTopicPageParser.class);

    @Override
    public List<MonItem> getMonItems() {
        try {
            Elements jobPrevItems = doc.select(".page_content .cols_table .row");

            List<MonItem> res = new ArrayList<MonItem>();

            int idx = 0;
            for (Element jobPrevItem : jobPrevItems) {
                if (idx++ == 0){
                    // Skipping header
                    continue;
                }
                Element headerElLink = jobPrevItem.select(".title a").first();

                MonItem item = new MonItem();
                item.setTitle(StringUtils.trim(headerElLink.text()));
                String href = headerElLink.attr("href");
                if (!StringUtils.startsWith(href, relativeUrlPrefix)) {
                    href = relativeUrlPrefix + href;
                }
                item.setUrl(href);

                res.add(item);
            }

            return res;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean isTopicMonItemsMustHaveBody() {
        return false;
    }
}
