package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;
import com.github.snowindy.util.text.StringUtil;

public class GuruComSiteTopicParser extends AbstractXmlParser implements ITopicPageParser {

    private final static Logger logger = LoggerFactory.getLogger(GuruComSiteTopicParser.class);

    @Override
    public List<MonItem> getMonItems() {
        try {
            Elements elements = doc.select(".serviceItem");

            List<MonItem> res = new ArrayList<MonItem>();

            for (Element el : elements) {
                Element headerElLink = el.select("h2>a").first();

                MonItem item = new MonItem();
                item.setTitle(StringUtil.whitespaceClenup(StringUtils.trim(headerElLink.text())));
                item.setUrl(relativeUrlPrefix + StringUtils.trim(headerElLink.attr("href")));

                Element descrEl = el.select(".desc").first();

                String descrText = StringUtil.escapeHtmlWithNewLineSupport(descrEl.html());
                descrText = replaceHtmlSpecials(descrText);
                descrText += "... [Marketplace requires login to view the rest of the job]";

                String catText = getCatText(el);

                if (StringUtils.isNotBlank(catText)) {
                    descrText += DESCR_SECTION_SEPARATOR + catText;
                }

                item.setTextBody(descrText);

                res.add(item);
            }

            return res;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    private String getCatText(Element el) {
        Element skills = el.select(".skills").first();
        if (skills != null) {
            List<String> skill = new ArrayList<>();
            for (Element skEl : skills.select("li")) {
                skill.add(skEl.text());
            }
            if (!skill.isEmpty()) {
                return StringUtil.whitespaceClenup("Skills: " + StringUtils.join(skill, ", "));
            }
        }
        return null;
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean isTopicMonItemsMustHaveBody() {
        return true;
    }
}
