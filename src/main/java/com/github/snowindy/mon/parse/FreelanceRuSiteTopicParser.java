package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.mon.i.parse.ex.PageTemporarilyNotAvailableException;

public class FreelanceRuSiteTopicParser extends AbstractXmlParser implements ITopicPageParser {

    private final static Logger logger = LoggerFactory.getLogger(FreelanceRuSiteTopicParser.class);

    @Override
    public List<MonItem> getMonItems() throws NotAvailableException {
        try {

            detectUnavailablePage();

            Elements jobPrevItems = doc.select(".proj");
            Elements jobPrevItemsContest = doc.select(".well.well-small:has(.row-fluid)");

            List<MonItem> res = new ArrayList<MonItem>();

            boolean isContest = jobPrevItems.size() == 0 && jobPrevItemsContest.size() != 0;

            if (isContest) {
                jobPrevItems = jobPrevItemsContest;
            }

            for (Element jobPrevItem : jobPrevItems) {
                Element headerElLink = jobPrevItem.select(isContest ? "h4 a" : "a.ptitle").first();
                Element descrEl = jobPrevItem.select(isContest ? "p" : ".descr").first();

                MonItem item = new MonItem();
                item.setTitle((isContest ? "Конкурс: " : "") + StringUtils.trim(headerElLink.text()));
                item.setUrl(relativeUrlPrefix + headerElLink.attr("href"));
                item.setTextBody(descrEl.text());

                res.add(item);
            }

            return res;
        } catch (PageTemporarilyNotAvailableException e) {
            throw e;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    private static final String PAGE_TEMP_UNAVAILABLE_MSG = "The page is temporarily unavailable";

    private void detectUnavailablePage() throws PageTemporarilyNotAvailableException {
        if (StringUtils.countMatches(pageText, "<html") > 1) {
            logger.info("The page is temporarily unavailable with incorrect html doc. Source descriptor: "
                    + sourceDescriptor);
            throw new PageTemporarilyNotAvailableException(sourceDescriptor);
        }

        Element el = doc.select("title").first();
        if (el != null) {
            if (PAGE_TEMP_UNAVAILABLE_MSG.equals(StringUtils.trim(el.text()))) {
                throw new PageTemporarilyNotAvailableException(sourceDescriptor);
            }
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean isTopicMonItemsMustHaveBody() {
        // there are evidences of contests with no body, so false.
        return false;
    }
}
