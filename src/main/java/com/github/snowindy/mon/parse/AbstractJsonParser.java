package com.github.snowindy.mon.parse;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public abstract class AbstractJsonParser extends AbstractParser {

    protected JsonNode rootNode;

    @Override
    public void parse(String pageText, String sourceDescriptor) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            rootNode = mapper.readTree(pageText);
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }

    }

}
