package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MetadataKeys;
import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class WeblancerNetSiteItemParser extends AbstractXmlParser implements IMonItemPageParser {

    private final static Logger logger = LoggerFactory.getLogger(WeblancerNetSiteItemParser.class);

    @Override
    public MonItem getJob() throws NotAvailableException {
        try {
            MonItem monItem = new MonItem();

            Map<String, String> md = monItem.getMetadata();

            Element titleEl = doc.select(".page_header h1").first();
            Elements categoryEl = doc.select(".page_header h2");

            List<String> cats = new ArrayList<>();
            for (Element element : categoryEl) {
                cats.add(element.text());
            }

            doc.select(".cols_table .row>div").first().remove();
            doc.select(".cols_table .row>div>.pull-right").remove();
            Element descrEl = doc.select(".cols_table .row>div").first();

            Elements budgetEl = doc.select(".amount.title");

            monItem.setTitle(titleEl.text());
            StringBuilder descrTextBuilder = new StringBuilder();
            descrTextBuilder.append(StringUtil.escapeHtmlWithNewLineSupport(descrEl.html()));

            md.put(MetadataKeys.JOB_CATEGORY.name(), StringUtils.join(cats, ", "));

            if (!budgetEl.isEmpty()) {
                md.put(MetadataKeys.JOB_BUDGET.name(), budgetEl.text());
            }

            String descrText = descrTextBuilder.toString();
            descrText = replaceHtmlSpecials(descrText);
            monItem.setTextBody(descrText);

            return monItem;

        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
