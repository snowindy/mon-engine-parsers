package com.github.snowindy.mon.parse;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MetadataKeys;
import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;

public class OdeskApiTopicPageParser extends AbstractJsonParser implements ITopicPageParser {
    @Override
    public List<MonItem> getMonItems() {
        try {
            JsonNode jobsNode = rootNode.path("jobs");
            if (jobsNode.isMissingNode()) {
                throw new RuntimeException("Cannot find jobs node.");
            }
            Integer numResults = rootNode.path("paging").path("count").asInt();

            DecimalFormat df = new DecimalFormat("#.##");
            
            List<MonItem> jobsRes = new ArrayList<MonItem>();
            for (int i = 0; i < numResults; i++) {
                JsonNode jobNode = jobsNode.path(i);
                MonItem monItem = new MonItem();
                monItem.setTitle(textAtPath(jobNode, "title"));
                monItem.setUrl(textAtPath(jobNode, "url"));

                String descr = textAtPath(jobNode, "snippet");

                String jobTypeOut;

                Map<String, String> md = monItem.getMetadata();

                String jobType = textAtPath(jobNode, "job_type");
                String budget = textAtPath(jobNode, "budget");
                String category1 = textAtPath(jobNode, "category2");
                String category2 = textAtPath(jobNode, "subcategory2");

                List<String> skills = new ArrayList<String>();
                for (int j = 0; j < jobNode.path("skills").size(); j++) {
                    skills.add(jobNode.path("skills").get(j).asText());
                }

                md.put(MetadataKeys.JOB_TYPE.name(), jobType);
                md.put(MetadataKeys.JOB_BUDGET.name(), budget);
                md.put(MetadataKeys.JOB_FORMATTED_MAX_BUDGET.name(), StringUtils.isNoneBlank(budget) ? new BigDecimal(
                        budget).toString() : null);
                md.put(MetadataKeys.JOB_CATEGORY.name(),
                        String.format("%s / %s", StringUtils.defaultString(category1),
                                StringUtils.defaultString(category2)));
                md.put(MetadataKeys.JOB_SKILLS.name(), StringUtils.join(skills, ", "));
                md.put(MetadataKeys.JOB_DURATION.name(), textAtPath(jobNode, "duration"));
                md.put(MetadataKeys.JOB_WORKLOAD.name(), textAtPath(jobNode, "workload"));

                JsonNode clientNode = jobNode.path("client");

                md.put(MetadataKeys.CLIENT_COUNTRY.name(), textAtPath(clientNode, "country"));
                String feedback = textAtPath(clientNode, "feedback");
                if (feedback != null) {
                    md.put(MetadataKeys.CLIENT_FEEDBACK_RATING.name(), df.format(Double.parseDouble(feedback)));
                }
                md.put(MetadataKeys.CLIENT_REVIEWS_COUNT.name(), textAtPath(clientNode, "reviews_count"));
                md.put(MetadataKeys.CLIENT_JOBS_POSTED.name(), textAtPath(clientNode, "jobs_posted"));
                md.put(MetadataKeys.CLIENT_PAST_HIRES.name(), textAtPath(clientNode, "past_hires"));
                md.put(MetadataKeys.CLIENT_PAYMENT_VERIFICATION_STATUS.name(),
                        textAtPath(clientNode, "payment_verification_status"));

                md.put(MetadataKeys.JOB_TYPE.name(), jobType);

                if ("Fixed".equals(jobType)) {
                    jobTypeOut = "fixed price";
                } else if ("Hourly".equals(jobType)) {
                    jobTypeOut = "hourly";
                } else {
                    throw new IllegalStateException(String.format("Unknown job type '%s'.", jobType));
                }

                descr = descr + "\n\nPayment type: " + jobTypeOut + "."
                        + (StringUtils.isNoneBlank(budget) ? " Max budget: " + budget + " USD." : "");

                monItem.setTextBody(descr);
                jobsRes.add(monItem);
            }

            return jobsRes;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    private String textAtPath(JsonNode node, String path) {
        node = node.path(path);
        if (node.isNull() || node.isMissingNode()) {
            return null;
        }
        return StringUtils.trimToNull(node.asText());
    }

    private final static Logger logger = LoggerFactory.getLogger(OdeskApiTopicPageParser.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean isTopicMonItemsMustHaveBody() {
        return true;
    }

}
