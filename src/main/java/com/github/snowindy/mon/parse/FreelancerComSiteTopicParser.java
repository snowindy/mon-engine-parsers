package com.github.snowindy.mon.parse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;

public class FreelancerComSiteTopicParser extends AbstractParser implements ITopicPageParser {
    @Override
    public List<MonItem> getMonItems() {
        try {
            List<MonItem> jobsRes = new ArrayList<MonItem>();
            for (JsonNode jobArr : rootNode) {
                MonItem monItem = new MonItem();
                monItem.setTitle(jobArr.get(1).asText().trim());
                monItem.setUrl(relativeUrlPrefix + jobArr.get(21).asText().trim());
                monItem.setTextBody(jobArr.get(2).asText().trim());
                jobsRes.add(monItem);
            }
            return jobsRes;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    protected JsonNode rootNode;

    @Override
    public void parse(String pageText, String sourceDescriptor) {

        try{
            String json = pageText.substring(pageText.indexOf("var aaData = [[") + "var aaData = ".length());
            json = json.substring(0, json.indexOf("\n"));
            json = json.trim();
            json = StringUtils.stripEnd(json, ";");

            ObjectMapper mapper = new ObjectMapper();
            this.rootNode = mapper.readTree(json);
        }catch (Exception e){
            throw new RuntimeException(e);
        }

    }


    private final static Logger logger = LoggerFactory.getLogger(FreelancerComSiteTopicParser.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean isTopicMonItemsMustHaveBody() {
        return true;
    }

}
