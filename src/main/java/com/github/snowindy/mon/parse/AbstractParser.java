package com.github.snowindy.mon.parse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.github.snowindy.mon.i.parse.IParser;

public abstract class AbstractParser implements IParser {

    protected String pageText;
    protected String sourceDescriptor;

    protected static String DESCR_SECTION_SEPARATOR = "\n\n";

    // TODO maybe jsoup can do it also?
    // TODO move to utils
    protected String replaceHtmlSpecials(String input) {
        input = StringUtils.replace(input, "&quot;", "\"");
        input = StringUtils.replace(input, "&apos;", "'");
        return input;
    }

    protected void logParseError(Exception e) {
        getLogger()
                .warn("Cannot parse page from '{}'. Page text: \n---------------------------------------------\n{}\n---------------------------------------------",
                        sourceDescriptor, pageText, e);
    }

    protected abstract Logger getLogger();

    protected String relativeUrlPrefix;

    @Override
    public void setRelativeUrlPrefix(String prefix) {
        this.relativeUrlPrefix = prefix;
    }

    @Override
    public void parse(String pageText, String sourceDescriptor) {
        this.pageText = pageText;
        this.sourceDescriptor = sourceDescriptor;

    }

}
