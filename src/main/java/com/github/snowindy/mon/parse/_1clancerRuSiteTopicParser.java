package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class _1clancerRuSiteTopicParser extends AbstractXmlParser implements
		ITopicPageParser {
	
	private final static Logger logger = LoggerFactory.getLogger(_1clancerRuSiteTopicParser.class);

	@Override
	public List<MonItem> getMonItems() throws NotAvailableException {
		try {
			
			Elements jobPrevItems = doc.select("ul#addform_place > li");
            
            List<MonItem> res = new ArrayList<MonItem>();
            
            for (Element jobPrevItem : jobPrevItems) {
                Element headerElLink = jobPrevItem.select("h3 > a").first();

                MonItem prev = new MonItem();
                String title = headerElLink.text();
				prev.setTitle(StringUtil.whitespaceClenup(StringUtils.trim(title)));
                prev.setUrl(this.relativeUrlPrefix + headerElLink.attr("href"));                

                res.add(prev);
            }

            return res;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
	}

	@Override
	public boolean isTopicMonItemsMustHaveBody() {
		return false;
	}

	@Override
	protected Logger getLogger() {
		return logger;
	}

}
