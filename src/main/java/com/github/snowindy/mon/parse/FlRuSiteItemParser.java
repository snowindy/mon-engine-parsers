package com.github.snowindy.mon.parse;

import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class FlRuSiteItemParser extends AbstractXmlParser implements IMonItemPageParser {

    private final static Logger logger = LoggerFactory.getLogger(FlRuSiteItemParser.class);

    @Override
    public MonItem getJob() throws NotAvailableException {
        try {
            MonItem job = new MonItem();

            boolean isContest = !doc.select(".contest-view").isEmpty();

            Element descrEl = null;

            if (isContest) {
                Element title = doc.select(".b-page__title").first();
                job.setTitle("Конкурс: " + replaceHtmlSpecials(title.text()));

                doc.select(".const-head .contest-body").remove();
                descrEl = doc.select(".contest-body div").first();
            } else {
                Element title = doc.select("[id^=prj_name_]").first();
                job.setTitle(replaceHtmlSpecials(title.text()));

                descrEl = doc.select("[id^=project_info_] [id^=projectp]").first();
            }

            String descrText = StringUtil.escapeHtmlWithNewLineSupport(descrEl.html());
            descrText = replaceHtmlSpecials(descrText);

            job.setTextBody(descrText);

            return job;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
