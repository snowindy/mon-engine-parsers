package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class GetacoderComSiteItemParser extends AbstractXmlParser implements
		IMonItemPageParser {
	
	private final static Logger logger = LoggerFactory.getLogger(GetacoderComSiteItemParser.class);

	@Override
	public MonItem getJob() throws NotAvailableException {
		try {
            MonItem job = new MonItem();

            String catText = getCategoriesText();

            Element title = doc.select("table:eq(0) tr:eq(2) table:eq(0) td:has(p) td")
            					.first();
            job.setTitle(replaceHtmlSpecials(title.text()));

            String descrText = selectDescriptionText();
            
			descrText = StringUtil.escapeHtmlWithNewLineSupport(descrText);
            descrText = replaceHtmlSpecials(descrText);

            if (StringUtils.isNotBlank(catText)) {
                descrText += DESCR_SECTION_SEPARATOR + catText;
            }

            job.setTextBody(descrText);

            return job;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
	}

	private String getCategoriesText() {
		Elements categoryElements = doc.select("a.catmenu");
		List<String> names = new ArrayList<>();
		for (Element categoryElement : categoryElements) {
			names.add( categoryElement.text() );
		}
		String res = StringUtils.join(names, ", ");
        if (StringUtils.isNotBlank(res)) {
            return "Category: " + res;
        }
        return null;
	}

	private String selectDescriptionText() {
		Element descrEl = null;
		descrEl = doc.select("span#descr1").first();


		String descrText = descrEl.html();
		
		descrText = descrText.replaceFirst("(?s).*<hr [^>]*>", "");
		
		return descrText;
	}

	@Override
	protected Logger getLogger() {
		return logger;
	}

}
