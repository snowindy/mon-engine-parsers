package com.github.snowindy.mon.parse;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class FreelancejobRuSiteItemParser extends AbstractXmlParser implements
		IMonItemPageParser {
	
	private final static Logger logger = LoggerFactory.getLogger(FreelancejobRuSiteItemParser.class);

	@Override
	public MonItem getJob() throws NotAvailableException {
		try {
            MonItem job = new MonItem();

            Element descrEl = null;

            Element title = doc.select("h1").first();
            job.setTitle(replaceHtmlSpecials(title.text()));

            descrEl = doc.select("h1 ~ table").first();

            String descrText = descrEl.html();
            
            String catText = getCatText(descrEl);
            
            descrText = removeFooter(descrText);
            descrText = StringUtil.escapeHtmlWithNewLineSupport(descrText);
            descrText = replaceHtmlSpecials(descrText);
            
            if (StringUtils.isNotBlank(catText)) {
                descrText += DESCR_SECTION_SEPARATOR + catText;
            }

            job.setTextBody(descrText);

            return job;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
	}

	private String getCatText(Element descrEl) {
		Element categoryElement = null;
		for (Element boldElement : descrEl.select("b")) {
			if ("Категория:".equals(boldElement.text())) {
				categoryElement = boldElement;
				break;
			}
		}
		if (categoryElement == null) {
			return "";
		}
		return categoryElement.text() + categoryElement.nextSibling().toString();
	}

	private String removeFooter(String descrText) {
		int indexOfBold = descrText.indexOf("<b>");
		if (indexOfBold >= 0) {
			descrText = descrText.substring(0, indexOfBold);
		}
		return descrText;
	}

	@Override
	protected Logger getLogger() {
		return logger;
	}

}
