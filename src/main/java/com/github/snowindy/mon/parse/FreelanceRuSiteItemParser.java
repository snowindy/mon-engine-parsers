package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.InsufficientViewRightsItemException;
import com.github.snowindy.mon.i.parse.ex.ItemBlockedException;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class FreelanceRuSiteItemParser extends AbstractXmlParser implements IMonItemPageParser {

    private final static Logger logger = LoggerFactory.getLogger(FreelanceRuSiteItemParser.class);

    @Override
    public MonItem getJob() throws NotAvailableException {
        try {
            MonItem job = new MonItem();

            detectInsufficientRightsPage();
            detectItemBlocked();

            boolean isContest = doc.select("#ContestDescription").size() > 0;
            Element reqsEl = null;
            Element categoryEl = null;
            String descrTextPre = null;

            if (isContest) {
                job.setTitle(replaceHtmlSpecials("Конкурс: " + doc.select(".page-header h2").text()));
                Elements descrEls = doc.select("#ContestDescription .well");

                List<String> parts = new ArrayList<String>();
                for (Element descrEl : descrEls) {
                    String title = descrEl.select("h4").text();

                    descrEl.select("h4").first().remove();

                    String descr = StringUtil.escapeHtmlWithNewLineSupport(descrEl.html());

                    parts.add(title + DESCR_SECTION_SEPARATOR + descr);
                }
                descrTextPre = StringUtils.join(parts, DESCR_SECTION_SEPARATOR);

            } else {
                Element title = doc.select(".proj_title").first();
                job.setTitle(replaceHtmlSpecials(title.text()));

                Elements trEls = doc.select("#proj_table tr");
                Element descrEl = findNextEl(trEls, "Описание проекта:").select("p").first();
                descrTextPre = StringUtil.escapeHtmlWithNewLineSupport(descrEl.html());
                Element reqsElWrap = findNextEl(trEls, "Обязательные требования:");
                if (reqsElWrap != null) {
                    reqsEl = reqsElWrap.select("p").first();
                }
                categoryEl = doc.select(".c_2").first();
            }

            StringBuilder descrTextBuilder = new StringBuilder();
            descrTextBuilder.append(descrTextPre);
            if (categoryEl != null) {
                descrTextBuilder.append(DESCR_SECTION_SEPARATOR);
                descrTextBuilder.append("Специализация: ");
                descrTextBuilder.append(categoryEl.text());
            }
            if (reqsEl != null) {
                descrTextBuilder.append(DESCR_SECTION_SEPARATOR);
                descrTextBuilder.append("Требования: ");
                descrTextBuilder.append(reqsEl.text());
            }

            job.setTextBody(replaceHtmlSpecials(descrTextBuilder.toString()));

            return job;
        } catch (NotAvailableException e) {
            throw e;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    private Element findNextEl(Elements els, String searchText) {
        Element res = null;
        for (int i = 0; i < els.size(); i++) {
            if (els.get(i).text().contains(searchText)) {
                res = els.get(i + 1);
                break;
            }
        }
        return res;
    }

    private void detectInsufficientRightsPage() throws NotAvailableException {
        Element el = doc.select(".msg_notify").first();
        if (el != null) {
            if (el.text().contains("Доступ к этому проекту для базовых аккаунтов закрыт")) {
                throw new InsufficientViewRightsItemException(sourceDescriptor);
            }
        }

    }

    private void detectItemBlocked() throws ItemBlockedException {
        Element el = doc.select(".msg_error").first();
        if (el != null) {
            if (el.text().contains("Учётная запись заблокирована")
                    && el.text().contains("Невозможно отобразить проект")) {
                throw new ItemBlockedException(sourceDescriptor);
            }
        }

    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
