package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;

public class FreelansimRuSiteTopicParser extends AbstractXmlParser implements ITopicPageParser {

    private final static Logger logger = LoggerFactory.getLogger(FreelansimRuSiteTopicParser.class);

    @Override
    public List<MonItem> getMonItems() {
        try {
            Elements jobPrevItems = doc.select("#tasks_list .task");

            List<MonItem> res = new ArrayList<MonItem>();

            for (Element jobPrevItem : jobPrevItems) {
                Element headerElLink = jobPrevItem.select(".task__title a").first();

                MonItem item = new MonItem();
                item.setTitle(StringUtils.trim(headerElLink.text()));
                item.setUrl(relativeUrlPrefix + headerElLink.attr("href"));

                res.add(item);
            }

            return res;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean isTopicMonItemsMustHaveBody() {
        return false;
    }
}
