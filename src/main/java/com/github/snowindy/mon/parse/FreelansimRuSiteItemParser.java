package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class FreelansimRuSiteItemParser extends AbstractXmlParser implements IMonItemPageParser {

    private final static Logger logger = LoggerFactory.getLogger(FreelansimRuSiteItemParser.class);

    @Override
    public MonItem getJob() throws NotAvailableException {
        try {
            MonItem job = new MonItem();

            Elements reqsEls = null;
            String descrTextPre = null;

            Element title = doc.select(".task .task__title").first();
            job.setTitle(replaceHtmlSpecials(title.text()));

            Element descrEl = doc.select(".task .task__description").first();
            descrTextPre = StringUtil.escapeHtmlWithNewLineSupport(descrEl.html());
            reqsEls = doc.select(".tags a");

            StringBuilder descrTextBuilder = new StringBuilder();
            descrTextBuilder.append(descrTextPre);
            if (reqsEls != null && !reqsEls.isEmpty()) {
                descrTextBuilder.append(DESCR_SECTION_SEPARATOR);
                descrTextBuilder.append("Разделы: ");
                List<String> reqs = new ArrayList<String>();
                for (Element reqEl : reqsEls) {
                    reqs.add(replaceHtmlSpecials(reqEl.text()));
                }
                descrTextBuilder.append(StringUtils.join(reqs, ", "));
            }

            job.setTextBody(replaceHtmlSpecials(descrTextBuilder.toString()));

            return job;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
