package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;
import com.github.snowindy.util.text.StringUtil;

public class PeopleperhourComSiteTopicParser extends AbstractXmlParser implements ITopicPageParser {

    private final static Logger logger = LoggerFactory.getLogger(PeopleperhourComSiteTopicParser.class);

    @Override
    public List<MonItem> getMonItems() {
        try {
            Elements elements = doc.select("#job-listing-listview .items>div");

            List<MonItem> res = new ArrayList<MonItem>();

            for (Element el : elements) {
                Element headerElLink = el.select(".title a").first();

                MonItem item = new MonItem();
                item.setTitle(StringUtil.whitespaceClenup(StringUtils.trim(headerElLink.text())));
                item.setUrl(StringUtils.trim(headerElLink.attr("href")));

                res.add(item);
            }

            return res;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean isTopicMonItemsMustHaveBody() {
        return false;
    }
}
