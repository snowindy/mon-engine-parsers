package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.IMonItemPageParser;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;
import com.github.snowindy.util.text.StringUtil;

public class _1clancerRuSiteItemParser extends AbstractXmlParser implements
		IMonItemPageParser {
	
	private final static Logger logger = LoggerFactory.getLogger(_1clancerRuSiteItemParser.class);

	@Override
	public MonItem getJob() throws NotAvailableException {
		try {
            MonItem job = new MonItem();

            Element descrEl = null;

            Element title = doc.select("h1").first();
            job.setTitle(replaceHtmlSpecials(title.text()));

            descrEl = doc.select("#discusstitle ~ div").first();

            String descrText = StringUtil.escapeHtmlWithNewLineSupport(descrEl.html());
            descrText = replaceHtmlSpecials(descrText);
            
            String catText = getCategoriesText();
            
            if (StringUtils.isNotBlank(catText)) {
                descrText += DESCR_SECTION_SEPARATOR + catText;
            }
            
            job.setTextBody(descrText);

            return job;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
	}

	private String getCategoriesText() {
		List<String> names = new ArrayList<String>();
		names.addAll(Arrays.asList("1C", "1С"));
		
		Element categoryElement = doc.select("li.spez").first();
		if (categoryElement != null) {
			names.add(categoryElement.text());
		}
		String res = StringUtils.join(names, ", ");
		return "Разделы: " + res;
	}

	@Override
	protected Logger getLogger() {
		return logger;
	}

}
