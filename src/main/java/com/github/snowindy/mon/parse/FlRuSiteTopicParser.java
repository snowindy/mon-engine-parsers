package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;
import com.github.snowindy.util.text.StringUtil;

public class FlRuSiteTopicParser extends AbstractXmlParser implements ITopicPageParser {

    private final static Logger logger = LoggerFactory.getLogger(FlRuSiteTopicParser.class);

    @Override
    public List<MonItem> getMonItems() {
        try {
            Elements jobPrevItems = doc.select(".b-post");

            List<MonItem> res = new ArrayList<MonItem>();

            for (Element jobPrevItem : jobPrevItems) {
                Element headerElLink = jobPrevItem.select("h2 a").first();

                MonItem prev = new MonItem();
                prev.setTitle(StringUtil.whitespaceClenup(StringUtils.trim(headerElLink.text())));
                prev.setUrl(relativeUrlPrefix + headerElLink.attr("href"));

                res.add(prev);
            }

            return res;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean isTopicMonItemsMustHaveBody() {
        return false;
    }
}
