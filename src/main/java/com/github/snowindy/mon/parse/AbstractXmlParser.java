package com.github.snowindy.mon.parse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public abstract class AbstractXmlParser extends AbstractParser {

    protected Document doc;

    @Override
    public void parse(String pageText, String sourceDescriptor) {

        pageText = parseInner(pageText);
        try {
            doc = Jsoup.parse(pageText);
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }

    }

    public String parseInner(String pageContent){
        return pageContent;
    }
}
