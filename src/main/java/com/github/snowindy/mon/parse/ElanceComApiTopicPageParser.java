package com.github.snowindy.mon.parse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.snowindy.mon.entity.MetadataKeys;
import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ITopicPageParser;

public class ElanceComApiTopicPageParser extends AbstractJsonParser implements ITopicPageParser {
    @Override
    public List<MonItem> getMonItems() {
        try {
            JsonNode pageResults = rootNode.path("data").path("pageResults");
            if (pageResults.isMissingNode()) {
                throw new RuntimeException("Cannot find pageResults node.");
            }
            Integer numResults = rootNode.get("data").get("numResults").asInt();

            List<MonItem> jobsRes = new ArrayList<MonItem>();
            for (int i = 0; i < numResults; i++) {
                JsonNode jobNode = pageResults.get(i);
                MonItem monItem = new MonItem();
                monItem.setTitle(jobNode.get("name").asText());
                monItem.setUrl(jobNode.get("jobURL").asText());
                String descr = jobNode.get("description").asText();
                String jobTypeOut;
                Map<String, String> md = monItem.getMetadata();

                String jobType = jobNode.get("isHourly").asText();
                md.put(MetadataKeys.JOB_TYPE.name(), "0".equals(jobType) ? "Fixed" : "Hourly");
                md.put(MetadataKeys.JOB_BUDGET.name(), textAtPath(jobNode, "budget"));
                md.put(MetadataKeys.JOB_CATEGORY.name(), String.format("%s / %s",
                        StringUtils.defaultString(textAtPath(jobNode, "category")),
                        StringUtils.defaultString(textAtPath(jobNode, "subcategory"))));
                md.put(MetadataKeys.JOB_SKILLS.name(), textAtPath(jobNode, "keywords"));
                md.put(MetadataKeys.JOB_DURATION.name(), textAtPath(jobNode, "duration"));
                md.put(MetadataKeys.JOB_WORKLOAD.name(), textAtPath(jobNode, "workload"));
                md.put(MetadataKeys.JOB_TAGS.name(), "true".equals(textAtPath(jobNode, "isFeatured")) ? "Featured" : "");

                String maxBudget = null;
                String maxRate = null;

                if ("0".equals(jobType)) {
                    jobTypeOut = "fixed price";
                    maxBudget = jobNode.get("budgetMax").asText();
                    if (!"0".equals(maxBudget)) {
                        monItem.getMetadata().put(MetadataKeys.JOB_FORMATTED_MAX_BUDGET.name(), maxBudget);
                    }
                    String minBudget = jobNode.get("budgetMin").asText();
                    if (!"0".equals(minBudget)) {
                        monItem.getMetadata().put(MetadataKeys.JOB_FORMATTED_MIN_BUDGET.name(), minBudget);
                    }
                } else if ("1".equals(jobType)) {
                    jobTypeOut = "hourly";
                    maxRate = jobNode.get("hourlyRateMax").asText();
                    if (!"0".equals(maxRate)) {
                        monItem.getMetadata().put(MetadataKeys.JOB_FORMATTED_MAX_RATE.name(), maxRate);
                    }
                    String hourlyRateMin = jobNode.get("hourlyRateMin").asText();
                    if (!"0".equals(hourlyRateMin)) {
                        monItem.getMetadata().put(MetadataKeys.JOB_FORMATTED_MAX_RATE.name(), hourlyRateMin);
                    }
                } else {
                    throw new IllegalStateException(String.format("Unknown job type '%s'.", jobType));
                }

                md.put(MetadataKeys.CLIENT_AVATAR_URL.name(), textAtPath(jobNode, "clientImageURL"));
                md.put(MetadataKeys.CLIENT_CITY.name(), textAtPath(jobNode, "clientCity"));
                md.put(MetadataKeys.CLIENT_COUNTRY.name(), textAtPath(jobNode, "clientCountry"));
                md.put(MetadataKeys.CLIENT_COUNTRY_CODE.name(), textAtPath(jobNode, "clientCountryCode"));
                md.put(MetadataKeys.CLIENT_FEEDBACK_RATING.name(), textAtPath(jobNode, "clientRating"));
                md.put(MetadataKeys.CLIENT_GEO_STATE.name(), textAtPath(jobNode, "clientState"));
                md.put(MetadataKeys.CLIENT_JOBS_POSTED.name(), textAtPath(jobNode, "clientJobsPosted"));
                md.put(MetadataKeys.CLIENT_NAME.name(), textAtPath(jobNode, "clientName"));
                md.put(MetadataKeys.CLIENT_PAST_HIRES.name(), textAtPath(jobNode, "clientJobsAwarded"));
                md.put(MetadataKeys.CLIENT_PAYMENT_VERIFICATION_STATUS.name(),
                        "true".equals(textAtPath(jobNode, "clientVerifiedPayment")) ? "Verified" : "Not verified");
                md.put(MetadataKeys.CLIENT_REGISTER_DATE.name(), textAtPath(jobNode, "clientMemberSince"));
                md.put(MetadataKeys.CLIENT_TOTAL_SPENT.name(), textAtPath(jobNode, "clientTotalPurchased"));

                descr = descr
                        + "\n\nPayment type: "
                        + jobTypeOut
                        + "."
                        + (StringUtils.isNoneBlank(maxBudget) && !"0".equals(maxBudget) ? " Max budget: " + maxBudget
                                + " USD." : "")
                        + (StringUtils.isNoneBlank(maxRate) && !"0".equals(maxRate) ? " Max rate: " + maxRate
                                + " USD/hour." : "");

                monItem.setTextBody(descr);

                jobsRes.add(monItem);
            }

            return jobsRes;
        } catch (Exception e) {
            logParseError(e);
            throw new RuntimeException(e);
        }

    }

    private String textAtPath(JsonNode node, String path) {
        node = node.path(path);
        if (node.isNull() || node.isMissingNode()) {
            return null;
        }
        return StringUtils.trimToNull(node.asText());
    }

    private final static Logger logger = LoggerFactory.getLogger(ElanceComApiTopicPageParser.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean isTopicMonItemsMustHaveBody() {
        return true;
    }

}
