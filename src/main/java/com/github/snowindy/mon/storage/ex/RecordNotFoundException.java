package com.github.snowindy.mon.storage.ex;

@Deprecated
public class RecordNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public RecordNotFoundException(String recordId) {
		super(String.format("Record not found for id '%s'.", recordId));
	}

	public RecordNotFoundException(String searchParameter, Throwable throwable) {
		super(String.format("Record not found for:  '%s'.", searchParameter,
				throwable));
	}

}
