Title: WordPress Clone Plugin Needs Updates

-------------------------------------

Hello,

I am in need of a reliable, quality PHP developer / programmer. I had a WordPress plugin created that duplicates/clones Wordpress Blogs/Sites exactly as-is so it can then be uploaded to a new domain and look exactly as the one that was cloned.

The plugin has worked great for smaller, simple sites. However, for larger sites it seems to run into trouble and not import the clone as it should. While it imports all of the plugins and themes, they are not activate and most if not all of the settings are not imported. Also, the pages and posts do not import.

I would think that for someone who knows what they are doing this would be a pretty simple project.

Please provide details as to why you're the right person for this job. If you can provide samples of work you've already created that would be helpful.

I will give you access to the plugin if you would like to review. Simply respond to this project and let me know. You will need to agree to a Non Disclosure Agreement.

Please feel free to ask any questions.

Thank you.(ID: 39734503)

Payment type: fixed price. Max budget: 499 USD.

-------------------------------------

Budget: Less than $500
Category: IT & Programming / Blog Programming
Job type: Fixed
Max budget: 499
Min budget: 20
Skills: PHP  PHP5  WordPress  CakePHP  MySQL Administration

-------------------------------------

Avatar url: https://ws.elance.com/media/images/4.0/no-photo-64x80.jpg
City: Papillion
Client name: Client
Country code: US
Country: United States
Jobs posted: 76
Past hires: 70
Payment status: Verified
Rating: 4
Registration date: August    2005
State: Nebraska
Total spent: above $5000