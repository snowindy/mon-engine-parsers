Build an online podcasting web app and desktop solution. Most podcasts require a guest to use Skype/Facetime/etc with bad quality and a lot of variables. This idea works more like LogMeIn, Go ToMeeting, etc. 

1 - When the podcast host launches the "master" program, he/she creates a new episode, defines the parameters (guest names, recording quality level, episode number, other relevant information) gets a 9 digit code, and sends it to the guest.

2 - The guest logs on to www.xxxxxxxxx.com and inputs the code. A browser based audio recording application starts. He/she selects their microphone.

3 - The host's computer now has control over the hosts recording web app. He/she can start, stop, and adjust the settings of the guests web app recording. 

4 - When record is hit, both applications begin recording at the same time, and (if necessary) record a small timecode for later sync. The audio is kept as a temporary file on each individuals computer. Once recording is stopped, both files are then automatically named (information taken from hosts session info, i.e. "GuestName_Episode-#_Take-#.wav" and saved to a folder on the hosts computer.

5 - A basic mixing suite is provided for the host to polish the final product (faders, automation, compression, import file for intros and segments, and an option to upload direct to major podcast hosting sites is built in upon mix down.

Category: Engineering, Networking, Programming