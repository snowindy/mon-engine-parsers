package com.github.snowindy.mon.parse;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class FlRuSiteItemParserTest extends Assert {

    @Test
    public void test_getJob1() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/fl.ru/job-post1-page.html"),
                "UTF-8");
        String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/fl.ru/job-post1-page.html-exp.txt"), "UTF-8"), "\r", "");

        FlRuSiteItemParser parser = new FlRuSiteItemParser();
        parser.setRelativeUrlPrefix("https://www.fl.ru");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals("Frontend разработчик на AngularJS", job.getTitle());
        assertEquals(expectedTextBody, job.getTextBody());
    }

    @Test
    public void test_getProject2() throws Exception {
        String page = FileUtils.readFileToString(
                new File("src/test/resources/parser/fl.ru/job-contest-post1-page.html"), "UTF-8");
        String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/fl.ru/job-contest-post1-page.html-exp.txt"), "UTF-8"), "\r", "");

        FlRuSiteItemParser parser = new FlRuSiteItemParser();
        parser.setRelativeUrlPrefix("https://www.fl.ru");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals("Конкурс: Сайт визитка 3 страницы.", job.getTitle());
        assertEquals(expectedTextBody, job.getTextBody());
    }

}
