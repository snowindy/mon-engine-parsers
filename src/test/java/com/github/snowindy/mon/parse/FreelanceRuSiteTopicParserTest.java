package com.github.snowindy.mon.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ex.PageTemporarilyNotAvailableException;

public class FreelanceRuSiteTopicParserTest extends Assert {

    @Test
    public void test_getJobsProjects() throws Exception {

        String page = FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelance.ru/topic-project-search-result-page.html"), "UTF-8");

        FreelanceRuSiteTopicParser parser = new FreelanceRuSiteTopicParser();
        parser.setRelativeUrlPrefix("http://freelance.ru");

        parser.parse(page, "http://test.com");

        List<MonItem> previews = parser.getMonItems();

        assertEquals(25, previews.size());

        assertEquals("Верстка лендинга под готовый дизайн.", previews.get(3).getTitle());
        assertEquals("http://freelance.ru/projects/235495/", previews.get(3).getUrl());
        assertTrue(previews
                .get(3)
                .getTextBody()
                .contains(
                        "Нужно сверстать лендинг под ключ по готовому дизайну. Все данные предоставлю. Верстка по линейке! Нужно сделать за 1 день. Лендинг простой."));

        assertEquals("Доработка сайта www.4-444.ru", previews.get(8).getTitle());
        assertEquals("http://freelance.ru/projects/233279/", previews.get(8).getUrl());
        assertTrue(previews
                .get(8)
                .getTextBody()
                .contains(
                        "1. Необходимо, чтобы мы регулярно могли синхронизировать товары, находящиеся на нашем сайте www.4-444.ru со складской программой. При этом должны меняться только ЦЕ…"));
    }

    @Test
    public void test_getJobsContests() throws Exception {

        String page = FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelance.ru/topic-contest-search-result-page.html"), "UTF-8");

        FreelanceRuSiteTopicParser parser = new FreelanceRuSiteTopicParser();
        parser.setRelativeUrlPrefix("http://freelance.ru");

        parser.parse(page, "http://test.com");

        List<MonItem> previews = parser.getMonItems();

        assertEquals(10, previews.size());

        assertEquals("Конкурс: новый дизайн сайта интернет - магазина (воздухоочистители)", previews.get(0).getTitle());
        assertEquals("http://freelance.ru/contest/view/3815", previews.get(0).getUrl());
        assertTrue(previews.get(0).getTextBody().contains("Требуется новый дизайн страниц сайта интернет"));

        assertEquals("Конкурс: Переработка логотипа и фирменного стиля \"Home Station\"", previews.get(8).getTitle());
        assertEquals("http://freelance.ru/contest/view/3805", previews.get(8).getUrl());
        assertTrue(previews.get(8).getTextBody()
                .contains("Разработка логотипа и фирменного стиля для магазинов «Home Station»"));

    }

    @Test(expected = PageTemporarilyNotAvailableException.class)
    public void test_pageNotAvailable() throws Exception {

        String page = FileUtils.readFileToString(
                new File("src/test/resources/parser/freelance.ru/topic-page-not-available.html"), "UTF-8");

        FreelanceRuSiteTopicParser parser = new FreelanceRuSiteTopicParser();
        parser.setRelativeUrlPrefix("http://freelance.ru");

        parser.parse(page, "http://test.com");

        parser.getMonItems();
    }

}
