package com.github.snowindy.mon.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;

public class FreelancejobRuSiteTopicParserTest {
	
	@Test
	public void test_getJobs() throws Exception {
		List<MonItem> previews = parse("main-page.htm");
		
		assertEquals(20, previews.size());
		assertEquals("Ищем менеджера по снабжению (строительство)", previews.get(0).getTitle());
		assertEquals("http://www.freelancejob.ru/vacancy/33937/", previews.get(0).getUrl());
		
		assertEquals("Установить и настроить шаблон Опенкарт", previews.get(2).getTitle());
		assertEquals("http://www.freelancejob.ru/vacancy/33935/", previews.get(2).getUrl());
		
		assertEquals("Копирайтер на постоянную работу", previews.get(7).getTitle());
		assertEquals("http://www.freelancejob.ru/vacancy/33930/", previews.get(7).getUrl());
	}
	
	private List<MonItem> parse(String pageName) throws IOException, NotAvailableException {
		String page = FileUtils.readFileToString(new File("src/test/resources/parser/freelancejob.ru/" + pageName),
                "UTF-8");
		FreelancejobRuSiteTopicParser parser = new FreelancejobRuSiteTopicParser();
		parser.setRelativeUrlPrefix("http://www.freelancejob.ru");
		
		parser.parse(page, "http://test.com");
		
		return parser.getMonItems();
	}
	
}
