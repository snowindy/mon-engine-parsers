package com.github.snowindy.mon.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class OdeskApiTopicPageParserTest extends Assert {
    @Test
    public void test() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/odesk.com/api-result-page.json"),
                "UTF-8");

        OdeskApiTopicPageParser parser = new OdeskApiTopicPageParser();
        parser.setRelativeUrlPrefix("foo");
        parser.parse(page, "file:src/test/resources/parser/odesk.com/api-result-page.json");
        List<MonItem> jobs = parser.getMonItems();

        assertEquals(50, jobs.size());
        assertEquals("http://www.odesk.com/jobs/~015757deece57b7443", jobs.get(0).getUrl());

        String expectedTextBody = StringUtils.replace(
                FileUtils.readFileToString(new File("src/test/resources/parser/odesk.com/job-body-exp.txt"), "UTF-8"),
                "\r", "");

        assertEquals(expectedTextBody, StringUtils.replace(jobs.get(0).getPlainTextForm(), "\r", ""));
    }
}
