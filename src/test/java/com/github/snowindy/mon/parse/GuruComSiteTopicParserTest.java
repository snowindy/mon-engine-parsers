package com.github.snowindy.mon.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class GuruComSiteTopicParserTest extends Assert {

    @Test
    public void test_getJobs() throws Exception {

        String page = FileUtils.readFileToString(new File("src/test/resources/parser/guru.com/topic-page.html"),
                "UTF-8");

        GuruComSiteTopicParser parser = new GuruComSiteTopicParser();
        parser.setRelativeUrlPrefix("http://www.guru.com");

        parser.parse(page, "http://www.guru.com");

        List<MonItem> previews = parser.getMonItems();

        assertEquals(20, previews.size());

        assertEquals("Social Media Integration with vTiger.", previews.get(0).getTitle());
        assertEquals(
                "http://www.guru.com/jobs/social-media-integration-with-vtiger./1040819&ItemNo=1&SearchUrl=search.aspx?",
                previews.get(0).getUrl());
        assertTrue(previews
                .get(0)
                .getTextBody()
                .contains(
                        "Social Media Integration with vTiger.in the form of a module.... [Marketplace requires login to view the rest of the job]"));
        assertTrue(previews
                .get(0)
                .getTextBody()
                .contains(
                        "Skills: Web, Software & IT, vtiger, social media integration, website development, website updating, module"));

        assertEquals("Marketing Executive, Web Developers,", previews.get(2).getTitle());
        assertEquals(
                "http://www.guru.com/jobs/marketing-executive-web-developers-/1040806&ItemNo=3&SearchUrl=search.aspx?",
                previews.get(2).getUrl());
        assertTrue(previews.get(2).getTextBody()
                .contains("from new clients. To creat... [Marketplace requires login to view the rest of the job]"));
        assertTrue(previews.get(2).getTextBody()
                .contains("Skills: Sales & Marketing, business development, direct sales, marketing, sai, sales"));
    }

}
