package com.github.snowindy.mon.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class WeblancerNetTopicPageParserTest extends Assert {

    @Test
    public void test_getJobs() throws Exception {

        String page = FileUtils.readFileToString(new File(
                "src/test/resources/parser/weblancer.net/topic-search-result-page.html"), "UTF-8");

        WeblancerNetTopicPageParser parser = new WeblancerNetTopicPageParser();
        parser.setRelativeUrlPrefix("https://www.weblancer.net");

        parser.parse(page, "http://test.com");

        List<MonItem> previews = parser.getMonItems();

        for (MonItem monItem : previews) {
            System.out.println(monItem.getTitle());
        }
        
        assertEquals(19, previews.size());

        // featured item
        assertEquals("Доработка проекта на CodeIgniter", previews.get(0).getTitle());
        assertEquals("https://www.weblancer.net/projects/veb-programmirovanie-31/dorabotka-proekta-na-codeigniter-789190/", previews.get(0).getUrl());

        // regular item
        assertEquals("Ведение развлекательного сайта", previews.get(3).getTitle());
        assertEquals("https://www.weblancer.net/vacancies/napolnenie-sajtov-35/vedenie-razvlekatelynogo-sajta-828039/", previews.get(3).getUrl());
    }

}
