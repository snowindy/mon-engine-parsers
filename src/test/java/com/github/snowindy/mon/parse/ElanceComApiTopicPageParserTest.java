package com.github.snowindy.mon.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class ElanceComApiTopicPageParserTest extends Assert {

    @Test
    public void test_getJobs() throws Exception {

        String page = FileUtils.readFileToString(
                new File("src/test/resources/parser/elance.com/api-json-jobs-list.txt"), "UTF-8");

        ElanceComApiTopicPageParser parser = new ElanceComApiTopicPageParser();
        parser.setRelativeUrlPrefix("https://www.elance.com");

        parser.parse(page, "http://test.com");

        List<MonItem> previews = parser.getMonItems();

        assertEquals(25, previews.size());

        assertEquals("PERL/PHP CORS Programmer Needed", previews.get(0).getTitle());
        assertEquals("https://www.elance.com/j/perl-php-cors-programmer-needed/40515215/", previews.get(0).getUrl());

        String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/elance.com/job-body-exp-0.txt"), "UTF-8"), "\r", "");
        assertEquals(expectedTextBody, StringUtils.replace(previews.get(0).getPlainTextForm(), "\r", ""));

        expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/elance.com/job-body-exp-1.txt"), "UTF-8"), "\r", "");
        assertEquals(expectedTextBody, StringUtils.replace(previews.get(1).getPlainTextForm(), "\r", ""));

        expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/elance.com/job-body-exp-2.txt"), "UTF-8"), "\r", "");
        assertEquals(expectedTextBody, StringUtils.replace(previews.get(2).getPlainTextForm(), "\r", ""));
    }
}
