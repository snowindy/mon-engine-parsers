package com.github.snowindy.mon.parse;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ex.InsufficientViewRightsItemException;
import com.github.snowindy.mon.i.parse.ex.ItemBlockedException;

public class FreelanceRuSiteItemParserTest extends Assert {

    @Test
    public void test_getMonItemProjectWithAttachments() throws Exception {
        String page = FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelance.ru/job-post-with-attachment-page.html"), "UTF-8");

        FreelanceRuSiteItemParser parser = new FreelanceRuSiteItemParser();
        parser.setRelativeUrlPrefix("http://freelance.ru");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals("Требуется кроссбраузерная \"резиновая\" верстка страницы.", job.getTitle());
        assertTrue(job.getTextBody().contains("Страница без привязки к CMS.\nПоддержка следующих браузеров"));
        assertTrue(job.getTextBody().contains("верстальщики знают:)"));
        assertTrue(job.getTextBody().contains("IT и Программирование: Скрипты / Web-приложения"));

        assertFalse(job.getTextBody().contains("<br />"));

        // attachments
        assertFalse(job.getFullText().contains("http://freelance.ru/download/?id=312557"));
    }

    @Test
    public void test_getMonItemProjectWithNoReqs() throws Exception {
        String page = FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelance.ru/job-post-with-no-requirements.html"), "UTF-8");

        FreelanceRuSiteItemParser parser = new FreelanceRuSiteItemParser();
        parser.setRelativeUrlPrefix("http://freelance.ru");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals("Разработка сервиса под ключ", job.getTitle());
        assertTrue(job.getTextBody().contains(
                "Требуется разработать онлайн-сервис под ключ. От дизайна до самого запуска. \nНаправление: SMM"));
        assertTrue(job.getTextBody().contains("Веб-дизайн: Сайт \"под ключ\""));
    }

    @Test(expected = ItemBlockedException.class)
    public void test_getMonItemProjectNotAvailableUserBlocked() throws Exception {
        String page = FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelance.ru/job-project-user-blocked.html"), "UTF-8");

        FreelanceRuSiteItemParser parser = new FreelanceRuSiteItemParser();
        parser.setRelativeUrlPrefix("http://freelance.ru");

        parser.parse(page, "http://test.com");

        parser.getJob();
    }

    @Test(expected = ItemBlockedException.class)
    public void test_getMonItemProjectItemBlockedRulesViolation() throws Exception {
        String page = FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelance.ru/job-project-item-blocked-rules-violation.html"), "UTF-8");

        FreelanceRuSiteItemParser parser = new FreelanceRuSiteItemParser();
        parser.setRelativeUrlPrefix("http://freelance.ru");

        parser.parse(page, "http://test.com");

        parser.getJob();
    }

    @Test(expected = InsufficientViewRightsItemException.class)
    public void test_getMonItemInsufficientRights() throws Exception {
        String page = FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelance.ru/job-project-not-available-page.html"), "UTF-8");

        FreelanceRuSiteItemParser parser = new FreelanceRuSiteItemParser();
        parser.setRelativeUrlPrefix("http://freelance.ru");

        parser.parse(page, "http://test.com");

        parser.getJob();
    }

    @Test
    public void test_getMonItemContest() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/freelance.ru/job-contest.html"),
                "UTF-8");

        FreelanceRuSiteItemParser parser = new FreelanceRuSiteItemParser();
        parser.setRelativeUrlPrefix("http://freelance.ru");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals("Конкурс: Название торговой марки сухих строительных смесях", job.getTitle());
        assertTrue(job.getTextBody().contains("Описание конкурса\n"));
        assertTrue(job.getTextBody().contains("Название торговой марки сухих строительных смесей"));
        assertTrue(job.getTextBody().contains("\nДополнительные сведения\n"));
        assertTrue(job.getTextBody().contains("звучание должно быть"));
        assertTrue(job.getTextBody().contains("\nБриф\n"));
        assertTrue(job.getTextBody().contains("Какое написание приоритетно для названия"));
        assertTrue(job.getTextBody().contains("масштабность, престижность, индивидуальность"));
    }

    @Test
    public void test_getMonItemContestNoBrief() throws Exception {
        String page = FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelance.ru/job-contest-no-brief.html"), "UTF-8");

        FreelanceRuSiteItemParser parser = new FreelanceRuSiteItemParser();
        parser.setRelativeUrlPrefix("http://freelance.ru");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals("Конкурс: Разработка подарочной упаковки для ВОДКИ «С СЕРЕБРОМ ПРЕМИУМ»", job.getTitle());
        assertTrue(job.getTextBody().contains("Описание конкурса\n"));
        assertTrue(job.getTextBody().contains("Водка «с Серебром Премиум» - первая и единственная"));
        assertTrue(job.getTextBody().contains("\nДополнительные сведения\n"));
        assertTrue(job.getTextBody().contains("Подарочная коробка предназначена"));
    }

}
