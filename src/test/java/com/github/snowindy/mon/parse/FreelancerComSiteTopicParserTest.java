package com.github.snowindy.mon.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class FreelancerComSiteTopicParserTest extends Assert {
    @Test
    public void test() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/freelancer.com/topic-page.html"),
                "UTF-8");

        FreelancerComSiteTopicParser parser = new FreelancerComSiteTopicParser();
        parser.setRelativeUrlPrefix("http://www.freelancer.com");
        parser.parse(page, "path");
        List<MonItem> jobs = parser.getMonItems();

        assertEquals(100, jobs.size());
        assertEquals("Build a Website or application", jobs.get(0).getTitle());
        assertEquals("http://www.freelancer.com/projects/html/Build-Website-application/",
                jobs.get(0).getUrl());
        assertEquals("Hi All \n" +
                        "\n" +
                        "I have 80 machines attached with server need to create one application which have below functionality\n" +
                        "- Need to moniter status of all machines(Status like Machine shutdown, hang,LAn issue to coonect with server)\n" +
                        "- Need to moniter status of two application which are working in every 80 machines (status like hang/notReposnding, offline, online)\n" +
                        "- Need to moniter DB in server like DB is...",
                jobs.get(0).getTextBody().replace("\r", ""));

        assertEquals("create a software", jobs
                .get(3).getTitle());
        assertEquals(
                "http://www.freelancer.com/projects/Internet-Marketing/amazon-listing-10865204/",
                jobs.get(2).getUrl());
    }
}
