package com.github.snowindy.mon.parse;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class PeopleperhourComSiteItemParserTest extends Assert {

    @Test
    public void test_getJob1() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/peopleperhour.com/job-page.html"),
                "UTF-8");

        PeopleperhourComSiteItemParser parser = new PeopleperhourComSiteItemParser();
        parser.setRelativeUrlPrefix("https://www.fl.ru");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/peopleperhour.com/job-page.html-exp.txt"), "UTF-8"), "\r", "");
        
        String textBody = job.getPlainTextForm();
        
        assertEquals(expectedTextBody, textBody);
    }
}
