package com.github.snowindy.mon.parse;

import static junit.framework.TestCase.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;

public class GetacoderComSiteTopicParserTest {
	
	@Test
	public void test_getJobs() throws IOException, NotAvailableException {
		String page = FileUtils.readFileToString(new File("src/test/resources/parser/getacoder.com/main-page.htm"),
                "UTF-8");
		GetacoderComSiteTopicParser parser = new GetacoderComSiteTopicParser();
		parser.parse(page, "http://test.com");
		List<MonItem> previews = parser.getMonItems();
		
		assertEquals(20, previews.size());
		assertEquals("Online Podcasting Web App", previews.get(0).getTitle());
		assertEquals("http://www.getacoder.com/projects/online_podcasting_web_app_166109.html", previews.get(0).getUrl());
		
		assertEquals("Craigslist Flagger Function", previews.get(2).getTitle());
		assertEquals("http://www.getacoder.com/projects/craigslist_flagger_function_166107.html", previews.get(2).getUrl());
		
		assertEquals("Online Food Ordering Website", previews.get(7).getTitle());
		assertEquals("http://www.getacoder.com/projects/online_food_ordering_website_166102.html", previews.get(7).getUrl());
	}
	
}
