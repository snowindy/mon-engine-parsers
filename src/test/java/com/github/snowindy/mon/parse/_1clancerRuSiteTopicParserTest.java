package com.github.snowindy.mon.parse;

import static junit.framework.TestCase.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;

public class _1clancerRuSiteTopicParserTest {
	
	@Test
	public void test_getJobs() throws IOException, NotAvailableException {
		String page = FileUtils.readFileToString(new File("src/test/resources/parser/1clancer.ru/main-page.htm"),
                "UTF-8");
		_1clancerRuSiteTopicParser parser = new _1clancerRuSiteTopicParser();
		parser.setRelativeUrlPrefix("http://1clancer.ru");
		parser.parse(page, "http://test.com");
		List<MonItem> previews = parser.getMonItems();
		
		assertEquals(30, previews.size());
		assertEquals("Проблемы с обменом БП 3.0 <-> Ут 10.3", previews.get(0).getTitle());
		assertEquals("http://1clancer.ru/offer/taskId=11410", previews.get(0).getUrl());
		
		assertEquals("Обновить нетиповую БП 3.0.31.14", previews.get(2).getTitle());
		assertEquals("http://1clancer.ru/offer/taskId=11407", previews.get(2).getUrl());
		
		assertEquals("Настроить обмен между БП 3 и УТ 3", previews.get(7).getTitle());
		assertEquals("http://1clancer.ru/offer/taskId=11401", previews.get(7).getUrl());
	}
	
}
