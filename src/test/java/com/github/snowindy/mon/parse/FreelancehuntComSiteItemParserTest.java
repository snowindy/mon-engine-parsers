package com.github.snowindy.mon.parse;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class FreelancehuntComSiteItemParserTest extends Assert {

    @Test
    public void test_getJob1() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/freelancehunt.com/job-post1.html"),
                "UTF-8");

        FreelancehuntComSiteItemParser parser = new FreelancehuntComSiteItemParser();
        parser.setRelativeUrlPrefix("http://freelancehunt.com");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals("Доработка Magento", job.getTitle());
        assertTrue(job.getTextBody().contains("Эту \"проблему\" решил сам.\n Есть"));
        assertTrue(job.getTextBody().contains("Magento:\n  в каталоге"));
        assertTrue(job.getTextBody().contains("(ссылка в корзину)\n нужно в каталоге"));
        assertTrue(job.getTextBody().contains("\nРазделы: HTML/XHTML, PHP, Веб-программирование, CMS"));
    }
}
