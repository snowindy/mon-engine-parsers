package com.github.snowindy.mon.parse;

import static junit.framework.TestCase.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;

public class FreelancejobRuSiteItemParserTest {
	@Test
	public void test_getJob() throws IOException, NotAvailableException {
		String page = FileUtils.readFileToString(new File("src/test/resources/parser/freelancejob.ru/job-post1-page.htm"),
                "UTF-8");
		String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelancejob.ru/job-post1-page.htm-exp.txt"), "UTF-8"), "\r", "");
		FreelancejobRuSiteItemParser parser = new FreelancejobRuSiteItemParser();
		parser.setRelativeUrlPrefix("http://www.freelancejob.ru");
		parser.parse(page, "http://test.com");
		
		MonItem job = parser.getJob();
		
		String textBody = job.getTextBody();
		
		assertEquals(expectedTextBody, textBody);
	}
}
