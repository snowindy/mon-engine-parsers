package com.github.snowindy.mon.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class FreelancehuntComSiteTopicParserTest extends Assert {

    @Test
    public void test_getJobs() throws Exception {

        String page = FileUtils.readFileToString(
                new File("src/test/resources/parser/freelancehunt.com/topic-jobs.html"), "UTF-8");

        FreelancehuntComSiteTopicParser parser = new FreelancehuntComSiteTopicParser();
        parser.setRelativeUrlPrefix("http://freelancehunt.com");

        parser.parse(page, "http://test.com");

        List<MonItem> previews = parser.getMonItems();

        assertEquals(15, previews.size());

        assertEquals("Тексты для сайта", previews.get(0).getTitle());
        assertEquals("http://freelancehunt.com/project/show/tekstyi-dlya-sayta/33252.html", previews.get(0).getUrl());

        assertEquals("Ищу копирайтера", previews.get(2).getTitle());
        assertEquals("http://freelancehunt.com/project/show/ischu-kopiraytera/33250.html", previews.get(2).getUrl());

        assertEquals("Сделать фильтр для программы Textpipe (или подобной)", previews.get(12).getTitle());
        assertEquals("http://freelancehunt.com/project/show/sdelat-filtr-dlya-programmyi-textpipe/33235.html", previews
                .get(12).getUrl());
    }

}
