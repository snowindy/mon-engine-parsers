package com.github.snowindy.mon.parse;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class GetacoderComSiteItemParserTest extends Assert {

    @Test
    public void test_getJob1() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/getacoder.com/post.htm"),
                "UTF-8");
        String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/getacoder.com/post.htm-exp.txt"), "UTF-8"), "\r", "");

        GetacoderComSiteItemParser parser = new GetacoderComSiteItemParser();
        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals(expectedTextBody, job.getTextBody());
    }
}
