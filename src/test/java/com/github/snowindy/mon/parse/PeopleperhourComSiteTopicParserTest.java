package com.github.snowindy.mon.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class PeopleperhourComSiteTopicParserTest extends Assert {

    @Test
    public void test_getJobs() throws Exception {

        String page = FileUtils.readFileToString(
                new File("src/test/resources/parser/peopleperhour.com/topic-page.html"), "UTF-8");

        PeopleperhourComSiteTopicParser parser = new PeopleperhourComSiteTopicParser();
        parser.setRelativeUrlPrefix("http://www.peopleperhour.com");

        parser.parse(page, "http://test.com");

        List<MonItem> previews = parser.getMonItems();

        assertEquals(20, previews.size());

        assertEquals("Web splash / landing page", previews.get(0).getTitle());
        assertEquals(
                "http://www.peopleperhour.com/job/web-splash-landing-page-723112",
                previews.get(0).getUrl());

        assertEquals("I need a professional app developer", previews.get(2).getTitle());
        assertEquals("http://www.peopleperhour.com/job/i-need-a-professional-app-developer-723110", previews.get(2).getUrl());
    }

}
