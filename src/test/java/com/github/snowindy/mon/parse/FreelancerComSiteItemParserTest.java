package com.github.snowindy.mon.parse;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class FreelancerComSiteItemParserTest extends Assert {

    @Test
    public void test_getJob1() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/freelancer.com/job-page.html"),
                "UTF-8");
        String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelancer.com/job-page.html-exp.txt"), "UTF-8"), "\r", "");

        FreelancerComSiteItemParser parser = new FreelancerComSiteItemParser();
        parser.setRelativeUrlPrefix("http://www.freelancer.com");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals(expectedTextBody, job.getPlainTextForm().replace("\r",""));
    }

}
