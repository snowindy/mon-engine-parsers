package com.github.snowindy.mon.parse;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class WeblancerNetSiteItemParserTest extends Assert {

    @Test
    public void test_getJob1() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/weblancer.net/job-page.html"),
                "UTF-8");
        String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/weblancer.net/job-page.html-exp.txt"), "UTF-8"), "\r", "");

        WeblancerNetSiteItemParser parser = new WeblancerNetSiteItemParser();
        parser.setRelativeUrlPrefix("https://weblancer.net");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals(expectedTextBody, job.getPlainTextForm());

    }

    @Test
    public void test_getJob2() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/weblancer.net/job-page-1.html"),
                "UTF-8");
        String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/weblancer.net/job-page-1.html-exp.txt"), "UTF-8"), "\r", "");

        WeblancerNetSiteItemParser parser = new WeblancerNetSiteItemParser();
        parser.setRelativeUrlPrefix("https://weblancer.net");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals(expectedTextBody, job.getPlainTextForm());

    }

}
