package com.github.snowindy.mon.parse;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class FreelansimRuSiteItemParserTest extends Assert {

    @Test
    public void test_getMonItemProjectWithLinks() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/freelansim.ru/job-post1-page.html"),
                "UTF-8");

        FreelansimRuSiteItemParser parser = new FreelansimRuSiteItemParser();
        parser.setRelativeUrlPrefix("http://freelansim.ru");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getJob();

        assertEquals("Дизайнер [Гуру web-интерфейсов]", job.getTitle());
        assertTrue(job.getTextBody().contains("Дорогие мои дизайнеры!"));
        assertTrue(job.getTextBody().contains("\nПожалуйста, кто не понимает, что это работа"));

        assertTrue(job.getTextBody().contains("\nРазделы: design, web design, ui design"));
    }

}
