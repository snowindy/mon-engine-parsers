package com.github.snowindy.mon.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;

public class FreelansimRuSiteTopicParserTest extends Assert {

    @Test
    public void test_getJobsProjects() throws Exception {

        String page = FileUtils.readFileToString(new File(
                "src/test/resources/parser/freelansim.ru/topic-project-search-result-page.html"), "UTF-8");

        FreelansimRuSiteTopicParser parser = new FreelansimRuSiteTopicParser();
        parser.setRelativeUrlPrefix("http://freelansim.ru");

        parser.parse(page, "http://test.com");

        List<MonItem> previews = parser.getMonItems();

        assertEquals(20, previews.size());

        assertEquals("Создание Flat иллюстраций для сайта", previews.get(2).getTitle());
        assertEquals("http://freelansim.ru/tasks/51650", previews.get(2).getUrl());
        
        assertEquals("SEO копирайтинг", previews.get(3).getTitle());
        assertEquals("http://freelansim.ru/tasks/51648", previews.get(3).getUrl());
    }
}
