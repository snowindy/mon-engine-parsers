package com.github.snowindy.mon.parse;

import com.github.snowindy.mon.entity.MonItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class FreeweblanceRuApiTopicPageParserTest extends Assert {
    @Test
    public void test_getJob1() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/freeweblance.ru/rss-page.xml"),
                "UTF-8");
        String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/freeweblance.ru/job-page.html-exp.txt"), "UTF-8"), "\r", "");

        FreeweblanceRuApiTopicPageParser parser = new FreeweblanceRuApiTopicPageParser();
        parser.setRelativeUrlPrefix("http://freeweblance.ru");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getMonItems().get(0);

        assertEquals(expectedTextBody, job.getPlainTextForm());
        assertEquals("http://freeweblance.ru/projnce./web-dev/novyj-lending", job.getUrl());

    }

    @Test
    public void test_getJob2() throws Exception {
        String page = FileUtils.readFileToString(new File("src/test/resources/parser/freeweblance.ru/rss-page.xml"),
                "UTF-8");
        String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/freeweblance.ru/job-page2.html-exp.txt"), "UTF-8"), "\r", "");

        FreeweblanceRuApiTopicPageParser parser = new FreeweblanceRuApiTopicPageParser();
        parser.setRelativeUrlPrefix("http://freeweblance.ru");

        parser.parse(page, "http://test.com");

        MonItem job = parser.getMonItems().get(1);

        assertEquals(expectedTextBody, job.getPlainTextForm());

    }
}
