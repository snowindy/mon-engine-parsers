package com.github.snowindy.mon.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.parse.FlRuSiteTopicParser;

public class FlRuSiteTopicParserTest extends Assert {

    @Test
    public void test_getJobs() throws Exception {

        String page = FileUtils.readFileToString(new File("src/test/resources/parser/fl.ru/topic-search-result-page.html"),
                "UTF-8");

        FlRuSiteTopicParser parser = new FlRuSiteTopicParser();
        parser.setRelativeUrlPrefix("https://www.fl.ru");

        parser.parse(page, "http://test.com");

        List<MonItem> previews = parser.getMonItems();

        assertEquals(30, previews.size());

        assertEquals("PR-менеджер", previews.get(0).getTitle());
        assertEquals("https://www.fl.ru/projects/1816394/pr-menedjer.html", previews.get(0).getUrl());

        assertEquals("Создать красивый продающий дизайн сайта (биотехнологии)", previews.get(2).getTitle());
        assertEquals(
                "https://www.fl.ru/projects/1811291/sozdat-krasivyiy-prodayuschiy-dizayn-sayta-biotehnologii.html",
                previews.get(2).getUrl());

        assertEquals("Доработка карточки товара для сайта на Opencart", previews.get(7).getTitle());
        assertEquals("https://www.fl.ru/projects/1816793/dorabotka-kartochki-tovara-dlya-sayta-na-opencart.html",
                previews.get(7).getUrl());
    }

    
    @Test
    public void test_getJobs_contest() throws Exception {

        String page = FileUtils.readFileToString(new File("src/test/resources/parser/fl.ru/topic-contests.html"),
                "UTF-8");

        FlRuSiteTopicParser parser = new FlRuSiteTopicParser();
        parser.setRelativeUrlPrefix("https://www.fl.ru");

        parser.parse(page, "http://test.com");

        List<MonItem> previews = parser.getMonItems();

        assertEquals(30, previews.size());

        assertEquals("DizKon: Логотип для хорошего бара", previews.get(0).getTitle());
        assertEquals("https://www.fl.ru/projects/1936556/dizkon-logotip-dlya-horoshego-bara.html", previews.get(0).getUrl());
    }

}
