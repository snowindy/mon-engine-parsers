package com.github.snowindy.mon.parse;

import static junit.framework.TestCase.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.github.snowindy.mon.entity.MonItem;
import com.github.snowindy.mon.i.parse.ex.NotAvailableException;

public class _1clancerRuSiteItemParserTest {
	@Test
	public void test_getJob() throws IOException, NotAvailableException {
		String page = FileUtils.readFileToString(new File("src/test/resources/parser/1clancer.ru/post.htm"),
                "UTF-8");
		String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/1clancer.ru/post.htm-exp.txt"), "UTF-8"), "\r", "");
		_1clancerRuSiteItemParser parser = new _1clancerRuSiteItemParser();
		parser.parse(page, "http://test.com");
		
		MonItem job = parser.getJob();
		
		String textBody = job.getTextBody();
		
		assertEquals(expectedTextBody, textBody);
	}
	
	@Test
	public void test_getJob_with_category() throws IOException, NotAvailableException {
		String page = FileUtils.readFileToString(new File("src/test/resources/parser/1clancer.ru/post-with-category.htm"),
                "UTF-8");
		String expectedTextBody = StringUtils.replace(FileUtils.readFileToString(new File(
                "src/test/resources/parser/1clancer.ru/post-with-category.htm-exp.txt"), "UTF-8"), "\r", "");
		_1clancerRuSiteItemParser parser = new _1clancerRuSiteItemParser();
		parser.parse(page, "http://test.com");
		
		MonItem job = parser.getJob();
		
		String textBody = job.getTextBody();
		
		assertEquals(expectedTextBody, textBody);
	}
}
